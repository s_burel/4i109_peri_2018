# Annexe : Commentaires, conseils et autres du Psesi 2018 :
## Dispositif de surveillance pour personne souffrant de troubles neurologiques

Cet Annexe comprend l’ensemble des conseils à destination de l’équipe qui souhaitera utiliser ou travailler sur le projet.

## Module
La fonction setup() ne contient qu’un appel à la fonction start_scheduler(), et la fonction loop() ne contient qu’un appel en boucle à la fonction scheduler_run().

### Fonction start_scheduler()
Cette fonction contient toutes les fonctions utilisées pour initialiser les composants.
Puis chaque tâche doit être ajoutées via la fonction add_task().
Tout le système tourne un petit ordonnanceur, qui contient un certain nombre de structure
countdown_handler.
Cette structure contient un compteur, un délai à atteindre et une routine à lancer en cas de
dépassement de délai.

### Fonction scheduler_run()
Cette fonction tourne en boucle, et parcourt le tableau de structure countdown_handler afin
de surveiller lesquelles doivent être lancées, avant bien sûr de les lancer.

### Du bon usage de l’ordonnanceur
Afin d’éviter de gaspiller de l’énergie, tous les composants à ajouter doivent avoir au moins
quatres fonctions :
- Une fonction pour régler les PIN utilisés par ce composant
- Une fonction pour allumer un composant
- Une fonction pour éteindre un composant
- Une fonction pour utiliser le composant (la tâche à utiliser)

### Sauvegarde de l’énergie
Toujours afin d’éviter de gaspiller de l’énergie, une tâche (routine reset()) a été ajoutée à l’ordonnanceur. Cette fonction éteint tous les composants qui peuvent l’être, avant de mettre en veille l’arduino pour un peu plus d’une minute. A la fin de la période de veille, l’arduino rallume tous les composants.

### Ajout d’un composant
Lors de l’ajout d’un composant, les modifications suivantes doivent être apportées au
programme :
- Ajout de variables globales au sein du fichier memory.h
- Ajout des pins utilisées au sein du fichier pin.h
- Ajout de la tâche au sein du scheduler (via la fonction start_scheduler()).
- Ajout de l’appel à la fonction d’initialisation dans la fonction start_scheduler()
- Ajout de l’appel aux fonctions pour allumer et éteindre le composant au sein de la fonction reset()
- Si besoin, modifications des fonctions de composants qui lisent les variables globales (écriture radio/port série/lcd)

### Communication module/base
La communication module/base se fait via le composant nRF24.
Comme ce dernier ne peut pas envoyer une chaîne de caractère de 32 bits, il a été décidé pour simplifier l’utilisation et la communication de coder toutes les informations relatives au module sur 32 bits.
En voici le format :
- 15 premiers bits (0 à 14) : Nom du module (e.g. “Chambre 1”
- 10 bits suivants (14 à 23) : Valeurs de 10 éventuels modules de mouvement (0 -> aucun mouvement, 1 -> mouvement détecté, x -> aucun détecteur de mouvement) (e.g. “01010xxxxx”)
- 4 bits suivants (24 à 27) : Valeur du capteur de son
- 4 bits suivants (28 à 31) : Valeur du capteur de lumière
Voici un exemple d’un message envoyé par le module : “Chambre 1
01010xxxxx123 12”

### Commentaire de code
Tout le code, disponible dans le répertoire sketch_projet/, est commenté.

## Base
Comme précisé dans le rapport, la base est composée de plusieurs programmes :
- Alerteur.c, qui vérifie chaque minute si une alerte doit être levée
- Reception.cpp, qui reçoit via le nRF24 des valeurs émises par les modules avant de les écrire dans un tube nommé /tmp/psesi.fifo
- Passeur.c, qui lit ce tube nommé avant d’écrire, une fois par minute, les dernières
valeurs des modules dans la base de donnée
- Un programme web feych.py, qui permet un interface entre l’utilisateur et la machine
- Un programme lanceur de courrier électronique, en shell

### La Base de Donnée
La base de donnée contient plusieurs tables :
- Une table module ayant pour chaque entrée une clé unique et le nom du module
correspondant
- Une table alertes ayant pour chaque entrée une clé unique, le courriel ou envoyer
l’alerte, le statut de l’alerte, le sujet du courriel envoyé et son contenu.
- Une table main, contenant une clé unique, une date, et pour 10 modules hypothétiques différents : 1 valeur pour le capteur de son, 1 valeur pour le capteur delumière, et 10 valeurs pour les capteurs de mouvements. Chaque entrée de la table main représente l’état global à un instant t de tous les modules du système

### Alerteur
Ce programme lit en boucle les alertes et leurs statuts au sein de la base de donnée. Il possède, pour chaque alerte, une fonction qui permet de décider si la condition de l’alerte est présente.
Chaque alerte a un statut, qui peut prendre plusieurs valeurs :
- 0 pour une alerte désactivée
- 1 pour une alerte activée qui sera désactivée après être lancée
- 2 pour une alerte activée qui sera encore activée même après être lancée
- >2 pour une alerte activée qui a été lancée récemment
Afin d’éviter les envois intempestifs du système en cas d’alerte continu, une temporisation a été mise en place : Une alerte ne peut pas être lancée deux fois en moins de 20 minutes.

Cette valeur peut être modifiée (c’est la macro TIME_TO_RESEND définie dans le fichier alert.h)

### Ajout d’une alerte
Si vous souhaitez ajouter une nouvelle alerte, vous devrez procéder de la sorte :
- Ajouter la description de l’alerte dans la table alertes de la base de donnée
- Ajouter une routine qui renvoie une valeur permettant de décider si l’alerte doit être lancée ou non
- Ajouter la routine de décision au sein du tableau de routine
does_function_must_be_run[](). Attention cependant ! l’ordre de ce tableau doit respecter celui de la base de donnée
- Recompilation du program alerteur.c et exécution du nouveau programme

### Réception
Le programme de réception doit être lancé avec, comme argument, l’adresse de canal de communication nRF24. Ce dernier peut fonctionner avec plusieurs modules.

### Script d’envoi de courriel
Le script d’envoi de courriel send_mail.sh reçoit comme argument l’index de l’alerte en envoyer dans la base de donnée. Ce dernier s’occupe du reste.

## Bibliothèque et outils nécessaire à l’usage du projet

### Git
sudo apt-get install git

### Flask
sudo apt-get install python-setuptools
sudo easy_install pip
sudo pip install flask

### NRF24
git clone https://github.com/nRF24/RF24.git
cd RF24
make
sudo make install
sudo raspi-config
-> Advanced Options
-> SPI
-> Yes

### Sqlite3
sudo apt-get install sqlite3
sudo apt-get install libsqlite3-dev

### Mail
sudo apt-get install msmtp-mta
#create ~/.msmtprc
chmod 600 .msmtprc
sudo apt-get install heirloom-mailx
#create ~/.mailrc

#### Fichier .msmtprc
#Gmail account
defaultslogfile ~/msmtp.log
account gmail
auth on
host smtp.gmail.com
from psesi2018@gmail.com
auth on
tls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
user psesi2018@gmail.com
password motdepassedesecuritemaximale
port 587
account default : gmail

#### Fichier .mailrc
set sendmail="/usr/bin/msmtp"
set message-sendmail-extra-arguments="-a gmail"

Attention ! Vérifier que le fichier /etc/ssl/certs/ca-certificates.crt existe. Sinon, trouvez un
fichier identique qui peut convenir (sudo find / -name “*certificates.crt*”)

## Problèmes initiaux, futurs idées
Tout ce dispositif a été conçu de sorte à ce qu’on puisse ajouter facilement jusqu’à 10 modules ayant chacun un grand nombres de capteurs. Si ce dispositif garde son but initial de surveillance au sein d’une maison médicalisée, cette situation est largement suffisante.

Cependant l’idée d’une intelligence artificielle pour détecter des alertes n’a pas pu être efficacement mise en place. Parmi les différentes méthodes possibles pour régler ce problème, la plus prometteuse semble être la logique floue.

Insérer cette méthode peut se faire via la modification du programme alerteur.c.

