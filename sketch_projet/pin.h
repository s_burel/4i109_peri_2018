#ifndef __PIN_H__
#define __PIN_H__ 0

#define NUMBER_OF_PIR          5
#define PIN_PIR_VCC            6
#define PIN_PIR2_VCC          A7
#define PIN_PIR0_OUT_VALUE     7
#define PIN_PIR1_OUT_VALUE     8
#define PIN_PIR2_OUT_VALUE    A2
#define PIN_PIR3_OUT_VALUE    A3
#define PIN_PIR4_OUT_VALUE    A6

#define PIN_LIGHT_OUT_ANALOG  A0
#define PIN_LIGHT_VCC          5

#define PIN_SOUND_OUT_ANALOG  A1
#define PIN_SOUND_VCC          4

#define PIN_LCD_VCC            8

#define TIME_TO_INIT_DEVICE 1000

#endif