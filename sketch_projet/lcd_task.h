#ifndef __LCD_TASK_H__
#define __LCD_TASK_H__ 0

#include "pin.h"
#include "memory.h"

/* LCD task functions */

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "memory.h"

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 
static const unsigned char PROGMEM logo16_glcd_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

/* Switch on the lcd via the arduino PIN used to supply the lcd energy */
void turn_on_lcd() {
  digitalWrite(PIN_LCD_VCC, HIGH);
  delay(TIME_TO_INIT_DEVICE);
  display.clearDisplay();  
}

/* Switch off the lcd via the arduino PIN used to supply the lcd energy */
void turn_off_lcd() {
  digitalWrite(PIN_LCD_VCC, LOW);
  display.ssd1306_command(SSD1306_DISPLAYOFF);
}

/* Configure the arduino PIN used to supply the lcd energy */
void set_up_lcd() {
  pinMode(PIN_LCD_VCC, OUTPUT);
  turn_on_lcd();
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
}

/**
 * Write a value to the lcd, global memory available (memory.h file) from
 * that function scope
 */
void write_lcd() {
  display.clearDisplay();
  /* Add stuff to do */
  display.display();
}

#endif
