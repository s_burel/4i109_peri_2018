/**
 * Minimalist scheduler
 */

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__ 0

#include <avr/wdt.h>
#include "LowPower.h"
#include "countdown_handler.h"
#include "led_task.h"
#include "pir_task.h"
#include "light_task.h"
#include "sound_task.h"
#include "tty_display.h"
#include "lcd_task.h"
#include "radio_task.h"
#include "config.h"

#define NUMBER_OF_TASK_MAX 10
int number_of_task = 0;

struct countdown_handler scheduler[NUMBER_OF_TASK_MAX];

/****************************************************************************
 * Note for good usage of the scheduler :                                   *
 * To save more energy, added component must get at least four functions :  *
 *   One function to set up the pin used by the component                   *
 *   One function to turn on  the alimentation of the component             *
 *   One function to turn off the alimentation of the component             *
 *   One function used to use the component                                 *
 ****************************************************************************/

/**
 * Add a task in the scheduler
 * params : void (*fn)()       : Task to add
 *          unsigned long time : Period of idle between two run of the task
 */
void add_task(void (*fn)(), unsigned long time) {
  task_set_period(&scheduler[number_of_task], time);
  task_set_handler(&scheduler[number_of_task], fn);
  number_of_task++;
}

/**
 * Reset fonction
 *
 * That function set the device in sleep mode.
 * To save more energy, that function turn off all of the device before
 * to set the Arduino in sleep mode. Of course, device then must be turned
 * on when the Arduino sleep is over 
 */
void reset() {
  /* Turn off the devices */
  TURN_OFF_PIR
  TURN_OFF_LGT
  TURN_OFF_SND
  switch_off_led();
  /* Sleep the arduino 64 seconds */
  for (int i = 0; i < 8; ++i) {
    /* Sleep for 8 seconds */
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  }
  /* Turn on the devices */
  TURN_ON_PIR
  TURN_ON_LGT
  TURN_ON_SND
}

/**
 * Function called once in set_up()
 *
 * All init function must me included in that function.
 * You must add routine to scheduler via the add_task function. These 
 * functions then will be runned once their delay (second argument of
 * add_task function) has occured.
 */
void start_scheduler() {
  /* Set up LED task */
  set_up_led();
  add_task(switch_led_state, 100);
  /* Set up PIR task */
  SET_UP_PIR
  /* Set Up Light task */
  SET_UP_LGT
  /* Set up TTY */
  set_up_serial();
  built_tty_memory();
  add_task(display_status, 1000);
  add_task(update_tty_memory, 1000);
  /* Set up Sound task */
  SET_UP_SND
  /* Set up LCD */
  // set_up_lcd();
  // add_task(write_lcd, 1000);
  /* Set up RADIO */
  turn_on_radio_writing();
  add_task(radio_write, 1000);  
  /* Set up RESET */
  // add_task(reset, 3000); 
}

/**
 * Look at all the task in scheduler and execute outdated ones
 *
 * Two function can be used to count the delay :
 * task_period_has_occured_ms and task_period_has_occured_us
 * One count the delays in milliseconds, other in microseconds
 */
void scheduler_run() {
  for (int i = 0; i < number_of_task; ++i) {
/*  if (task_period_has_occured_us(&scheduler[i])) {*/
    if (task_period_has_occured_ms(&scheduler[i])) {
      task_set_last_call(&scheduler[i], millis());
      task_execute(&scheduler[i]);
    }
  }
}

#endif