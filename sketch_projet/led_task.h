#ifndef __LED_TASK_H__
#define __LED_TASK_H__ 0

#include "memory.h"

/**
 * LED task function
 */

/* Configure the arduino PIN used to supply the led energy */
void set_up_led() {
  pinMode(LED_BUILTIN, OUTPUT);
}

/* Change the value of the led */
void switch_led_state() {
  arduino_led = !arduino_led;
  digitalWrite(LED_BUILTIN, arduino_led);
}

/* Turn off the led */
void switch_off_led() {
  arduino_led = 0;
  digitalWrite(LED_BUILTIN, arduino_led);
}

#endif