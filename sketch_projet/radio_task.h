#include <SPI.h>
#include "RF24.h"
#include "printf.h"
#include "config.h"

/* Radio tasks functions */

/* The radio must be powered by 3.3V pin of arduino or problems occurs */

RF24 radio(9,10);

byte addresses[][6] = {NRF_CANAL};

/* Turn on the radio and set it to listening mode */
void turn_on_radio_listening() {
  delay(TIME_TO_INIT_DEVICE);
  radio.begin();
  radio.setRetries(15,15);
  radio.setPALevel(RF24_PA_LOW);
  radio.openReadingPipe(1,addresses[0]);
  radio.printDetails();
  radio.startListening();
}

/* Turn on the radio and set it to writing mode */
void turn_on_radio_writing() {
  delay(TIME_TO_INIT_DEVICE);
  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio.openWritingPipe(addresses[0]);
}

/* Function deprecated since the radio can not be powered by arduino PIN */
void turn_off_radio() {
  // digitalWrite(PIN_RADIO_VCC, LOW);
}

/* Read the value from the radio and save it to global memory */
void radio_read() {
  if( radio.available()){
     radio.read( radio_buffer, sizeof(radio_buffer) );             // Get the payload
     Serial.println(radio_buffer);
   }
}

/* Send to the radio a string containing all the sensor values */
void radio_write() {
  char * offset;
  char value [5];

  /* Reset the string */                  /*                                */
  for (int i = 0; i < 32; ++i)
    radio_buffer[i] = ' ';

  /* Reset the offset */
  offset = radio_buffer;                  /*|                               */

  /* Set the name of the module */        /*Chambre 1                       */
  strcpy(offset, MODULE_NAME);
  offset += 15;                           /*Chambre 1      |                */

  /* Set the value of PIR */
  for (int i = 0; i < NUMBER_OF_PIR; ++i) {
    if(pir_value[i]) offset[i] = '1';
    else offset[i] = '0';
  }                                       /*Chambre 1      pir_values       */
  for (int i = NUMBER_OF_PIR; i < 10; ++i)
    offset[i]='x';                        /*Chambre 1      pir_values|      */
  offset += 10;

  /* Set the value of sound sensor */
  if (sound_value == 1000) sound_value = 999;
  snprintf(value, 4, "%d", sound_value);
  strcpy(offset, value);                  /*Chambre 1      pir_values|snd   */
  offset += 4;                            /*Chambre 1      pir_values snd|  */

  /* Set he value of light sensor */
  if (light_value == 1000) light_value = 999;
  snprintf(value, 4, "%d", light_value);
  strcpy(offset, value);                  /*Chambre 1      pir_values sndlgt*/

  /* Print the string to the Serial port */
  for (int i = 0; i < 32; ++i)
    Serial.print(radio_buffer[i]);
  Serial.print("\n");

  /* Send the string to the radio */
  radio.write( radio_buffer, 32 );
}