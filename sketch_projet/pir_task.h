#ifndef __PIR_TASK_H__
#define __PIR_TASK_H__ 0

#include "memory.h"
#include "pin.h"
#include "config.h"

/* PIR task functions */

/* Switch on the PIRs via the arduino PINs used to supply the energy */
void turn_on_pir() {
  digitalWrite(PIN_PIR2_VCC, HIGH);
  digitalWrite(PIN_PIR_VCC, HIGH);
}

/* Switch off the PIRs via the arduino PINs */
void turn_off_pir() {
  digitalWrite(PIN_PIR2_VCC, LOW);
  digitalWrite(PIN_PIR_VCC, LOW);
}

/* Configure the arduino PINs used to supply the energy of the PIRs */
void set_up_pir() {
  pinMode(PIN_PIR_VCC, OUTPUT);
  pinMode(PIN_PIR2_VCC, OUTPUT);

  SET_PIR_PIN

  turn_on_pir();
}

/**
 * Read the value from the motion sensor connected to the PINs, and save it
 * to the global array pir_value.
 * Global memory available (memory.h file) from that function scope
 */
void pir_read() {
  PIR_READ
}

#endif
