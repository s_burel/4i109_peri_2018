/**
 * Sound Task file
 */

#include "pin.h"
#include "memory.h"

#ifndef __SOUND_TASK_H__
#define __SOUND_TASK_H__ 0

/* Sound task functions */

/* Switch on sound sensor the via the arduino PIN used to supply the energy */
void turn_on_sound() {
  digitalWrite(PIN_SOUND_VCC, HIGH);
}

/* Switch off sound sendor the via the arduino PIN */
void turn_off_sound() {
  digitalWrite(PIN_SOUND_VCC, LOW);
}

/* Configure the arduino PIN used to supply the energy of the sound sensor */
void set_up_sound() {
  pinMode(PIN_SOUND_VCC, OUTPUT);
  pinMode(PIN_SOUND_OUT_ANALOG, INPUT);
  turn_on_sound();
}

/**
 * Read the value from the sound sensor connected to the PIN, and save it
 * to the global variable sound_value.
 * Global memory available (memory.h file) from that function scope
 */
void get_sound() {
  sound_value = analogRead(PIN_SOUND_OUT_ANALOG);
}

#endif