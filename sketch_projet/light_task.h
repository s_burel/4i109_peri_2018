#ifndef __LIGHT_TASK_H__
#define __LIGHT_TASK_H__ 0

#include "pin.h"
#include "memory.h"

/* Light task functions */

/* Switch on the light sensor via the arduino PIN used to supply the energy */
void turn_on_light() {
  digitalWrite(PIN_LIGHT_VCC, HIGH);
}

/* Switch off the light sensor via the arduino PIN */
void turn_off_light() {
  digitalWrite(PIN_LIGHT_VCC, LOW);
}

/* Configure the arduino PIN used to supply the energy of the light sensor */
void set_up_light() {
  pinMode(PIN_LIGHT_VCC, OUTPUT);
  pinMode(PIN_LIGHT_OUT_ANALOG, INPUT);
  turn_on_light();
}

/**
 * Read the value from the light sensor connected to the PIN, and save it
 * to the global variable light_value.
 * Global memory available (memory.h file) from that function scope
 */
void get_light() {
  light_value = analogRead(PIN_LIGHT_OUT_ANALOG);
}

#endif
