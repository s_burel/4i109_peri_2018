#ifndef __COUNTDOWN_HANDLER_H__
#define __COUNTDOWN_HANDLER_H__ 0

/**
 * struct countdown_handler
 * 
 * That struct contains all informations used to describe one task
 * The scheduler contains many struct of that types and run with its interface
 * unsigned long last_call : Last call of the handler
 * unsigned long period    : Time between two run of the task
 * void (*handler)()       : Task ran describing the task
 */
struct countdown_handler
{
  unsigned long last_call = 0; /* Last call of the handler */
  unsigned long period;        /* Countdown to handler */
  void (*handler)();           /* Handler to call */
};

/* CountDown_Handler setter : Set last_call */
void task_set_last_call(struct countdown_handler * tgt, unsigned long data) {
  tgt->last_call = data;
}

/* CountDown_Handler setter : Set period */
void task_set_period(struct countdown_handler * tgt, unsigned long data) {
  tgt->period = data;
}

/* CountDown_Handler setter : Set handler */
void task_set_handler(struct countdown_handler * tgt, void (*fn)()) {
  tgt->handler = fn;
}

/* CountDown_Handler getter : Get last call */
unsigned long task_last_call(struct countdown_handler * tgt) {
  return tgt->last_call;
}

/* CountDown_Handler getter : Get Period */
unsigned long task_period(struct countdown_handler * tgt) {
  return tgt->period;
}

/* CountDown_Handler getter : Execute handler */
void task_execute(struct countdown_handler * tgt) {
  return tgt->handler();
}

/**
 * Return the number of periods, that has occured
 * since the last call of the handler
 * Return 0 if no period has occured,
 * otherwise return a positive value
 */
unsigned int task_period_has_occured
(struct countdown_handler * tgt, unsigned long present) {
  unsigned long past = task_last_call(tgt);
  unsigned long lived = present - past;
  /* Detect an overflow */
  if (present < past)
    lived = present + (((unsigned long)((long) (-1))) - lived);
  return lived / task_period(tgt);
}

/**
 * Return the number of periods that has occured
 * since the last call of the handler, in milliseconds.
 * Return 0 if no period has occured,
 * otherwise return a positive value
 */
unsigned int task_period_has_occured_ms(struct countdown_handler * tgt) {
  unsigned long present = millis();
  return task_period_has_occured(tgt, present);
}

/**
 * Return the number of periods, in microsecond, that has occured
 * since the last call of the handler, in microseconds.
 * Return 0 if no period has occured,
 * otherwise return a positive value
 */
unsigned int task_period_has_occured_us(struct countdown_handler * tgt) {
  unsigned long present = micros();
  return task_period_has_occured(tgt, present);
}

#endif