#ifndef __TTY_DISPLAY_H__
#define __TTY_DISPLAY_H__ 0

#include "memory.h"

/* TTY display functions */

/* Set up the serial port */
void set_up_serial() {
    Serial.begin(9600);
}

/* Display the tty_variable values to the serial port */
void display_status() {
  for (int i = 0; i < TTY_SIZE; ++i)
    if (tty_text[i] != NULL) {
      Serial.print(tty_text[i]);
      Serial.println(tty_value[i]);
    }
}

/* Set the global variables tty_text used by the serial port tasks */
void built_tty_memory() {
  for (int i = 0; i < TTY_SIZE; ++i)
    tty_text[i] = NULL;
  tty_text[0] = (char * ) "Valeur du pir        : ";
  tty_text[1] = (char * ) "Valeur de la lumière : ";
  tty_text[2] = (char * ) "Valeur du son        : ";
}

/* Set the global variables tty_values used by the serial port tasks */
void update_tty_memory() {
  tty_value[0] = pir_value[0];
  tty_value[1] = light_value;
  tty_value[2] = sound_value;
}

#endif