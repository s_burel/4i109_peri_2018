#ifndef __GLOBAL_MEMORY__
#define __GLOBAL_MEMORY__ 0

#define TTY_SIZE 100

/* This file contains all the memory, global and common to all tasks */

/* LED task variables */
int arduino_led = 0; /* Value of the arduino_led */

/* PIR task variables */
int pir_value[10] = {0,0,0,0,0,0,0,0,0,0};

/* TTY variables : 1 char * and 1 int for each task */
char * tty_text[TTY_SIZE];
int tty_value[TTY_SIZE];

/* Light task variable */
int light_value = 0;

/* Sound task variable */
int sound_value = 0;

/* Radio variables */
char radio_buffer[32];

#endif