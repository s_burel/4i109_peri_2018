#ifndef __CONFIG_H__
#define __CONFIG_H__ 0
/* This file contains all the stuff configured by the config menu */

#define TURN_ON_LGT  turn_on_light();
#define TURN_OFF_LGT turn_off_light();
#define SET_UP_LGT   set_up_light(); add_task(get_light, 100);

#define TURN_ON_SND  turn_on_sound();
#define TURN_OFF_SND turn_off_sound();
#define SET_UP_SND   set_up_sound(); add_task(get_sound, 100);

#define TURN_ON_PIR  turn_on_pir();
#define TURN_OFF_PIR turn_off_pir();
#define SET_UP_PIR   set_up_pir(); add_task(pir_read, 1000);
#define SET_PIR_PIN  pinMode(PIN_PIR0_OUT_VALUE, INPUT); \
                     pinMode(PIN_PIR1_OUT_VALUE, INPUT); \
                     pinMode(PIN_PIR2_OUT_VALUE, INPUT); \
                     pinMode(PIN_PIR3_OUT_VALUE, INPUT); \
                     pinMode(PIN_PIR4_OUT_VALUE, INPUT);
#define PIR_READ     pir_value[0] = digitalRead(PIN_PIR0_OUT_VALUE); \
                     pir_value[1] = digitalRead(PIN_PIR1_OUT_VALUE); \
                     pir_value[2] = digitalRead(PIN_PIR2_OUT_VALUE); \
                     pir_value[3] = digitalRead(PIN_PIR3_OUT_VALUE); \
                     pir_value[4] = digitalRead(PIN_PIR4_OUT_VALUE);

#define MODULE_NAME  "Chambre 1"

#define NRF_CANAL    "Canal"

#endif