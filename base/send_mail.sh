# Script  shell utilisé pour envoyer un mail
# Recoit comme argument l'index de l'alerte dans la table alertes à envoyer
# Avant d'anvoyer un mail.
f=/tmp/alert_message
echo 'select message from alertes where id = ' $1 ';' | sqlite3 db > $f
courriel=`echo 'select mail from alertes where id = ' $1 ';' | sqlite3 db`
sujet=`echo 'select description from alertes where id = ' $1 ';' | sqlite3 db`
echo "Envoi de mail : fonction $1 ($sujet)"
echo "mail -s "$sujet" $courriel < $f"
mail -s "$sujet" $courriel < $f
rm $f