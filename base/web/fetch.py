from flask import Flask, render_template, request
import sql_request

app = Flask(__name__)

@app.route("/mod0_light.json")
def light0():
    return sql_request.select_one("mod0_light")

@app.route("/mod0_sound.json")
def sound():
    return sql_request.select_one("mod0_sound")

@app.route("/mod0_pir.json")
def pir():
    return sql_request.select_sum_of_pirs("mod0")

@app.route("/mod1_light.json")
def light2():
    return sql_request.select_one("mod1_light")

@app.route("/mod1_sound.json")
def sound2():
    return sql_request.select_one("mod1_sound")

@app.route("/mod1_pir.json")
def pir2():
    return sql_request.select_sum_of_pirs("mod1")

@app.route("/alerts.json")
def alerts():
    return sql_request.alerts()

@app.route('/sql_request')
def query():
    fn = request.args.get('id')
    v = request.args.get('value')
    return sql_request.set_alert(fn, v)

@app.route("/chambre1")
def chambre1():
    return render_template('chambre1.html')

@app.route("/chambre2")
def chambre2():
    return render_template('chambre2.html')

@app.route("/alertes")
def alertes():
    return render_template('alertes.html')

if __name__ == '__main__':
    app.run(
    debug=True,
    threaded=True,
    host='0.0.0.0'
)