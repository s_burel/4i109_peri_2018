from flask import Flask, render_template, request
import sql_request

app = Flask(__name__)

@app.route("/alerts.json")
def alerts():
    return sql_request.alerts()

@app.route('/sql_request')
def query():
    fn = request.args.get('id')
    v = request.args.get('value')
    return sql_request.set_alert(fn, v)

@app.route("/alertes")
def alertes():
    return render_template('alertes.html')

if __name__ == '__main__':
    app.run(
    debug=True,
    threaded=True,
    host='0.0.0.0'
)