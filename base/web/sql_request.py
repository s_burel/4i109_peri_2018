import calendar
import datetime
import time
import sqlite3
import json

# That function return a sql request including date and column given in
# parameter from the main table. The result is json-formatted
def select_one(column):
    connection = sqlite3.connect("../db")
    cursor = connection.cursor()
    request = "SELECT date, " + column + " from main"
    cursor.execute(request)
    _tuple = cursor.fetchall()
    # print _tuple
    _list = list(_tuple)
    for i in range (0,len(_list)):
        _list[i] = list(_list[i])
        date = datetime.datetime.strptime(_list[i][0], "%Y-%m-%d %H:%M:%S")
        _list[i][0] = calendar.timegm(date.utctimetuple()) * 1000 + 7200000
    # print (_list)
    return json.dumps(_list)

# That function return a sql request including date and sum of pirs
# from the module given in parameter from the main table.
# The result is json-formatted
def select_sum_of_pirs(module):
    connection = sqlite3.connect("../db")
    cursor = connection.cursor()
    # List of pirs :
    pirs = ["1","2","3","4","5","6","7","8","9"]
    # Pir0
    request = "SELECT date, " + module + "_pir0 from main"
    cursor.execute(request)
    _tuple = cursor.fetchall()
    _list = list(_tuple)
    # Set Date
    for i in range (0,len(_list)):
        _list[i] = list(_list[i])
        date = datetime.datetime.strptime(_list[i][0], "%Y-%m-%d %H:%M:%S")
        _list[i][0] = calendar.timegm(date.utctimetuple()) * 1000
    # Loop for each pirs :
    for j in pirs:
        request = "SELECT date, " + module + "_pir" + j + " from main"
        cursor.execute(request)
        _tuple = cursor.fetchall()
        # Set Date
        for i in range (0,len(_list)):
            if _list[i][1] is None:
                _list[i][1] = _tuple[i][1]
            else :
                if _tuple[i][1] is not None:
                    _list[i][1] += _tuple[i][1]
    return json.dumps(_list)

# Return value from alertes table json-formated
def alerts():
    print "Alerte json access"
    connection = sqlite3.connect("../db")
    cursor = connection.cursor()
    cursor.execute("SELECT * from alertes")
    _tuple = cursor.fetchall()
    print _tuple
    # print (_list)
    return json.dumps(_tuple)

# Set the value of an entry from the table alertes where 
#   fn is the alert to set
#   v  is the value to set
# Then return an HTML string displaying the query and a meta_link
# to redirect directly the user to the alertes html page
def set_alert(fn,v):
    string = "UPDATE alertes SET status = ";
    string += v
    string += " WHERE id = "
    string += fn
    string += ";"
    print string;
    connection = sqlite3.connect("../db")
    cursor = connection.cursor()
    cursor.execute(string);
    connection.commit();
    print ("set");
    return '''<meta http-equiv="refresh" content="0; url=/alertes" />
              <h1>Function {} set to {}</h1>'''.format(fn, v)
