#ifndef __SB_ALERT_H__
#define __SB_ALERT_H__ 0

#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <sqlite3.h>
#include <string.h>
#include <stdlib.h>

/* Define sensor types */
#define ST_PIR   0
#define ST_LIGHT 1
#define ST_SOUND 2

#define NUMBER_OF_ALERT     100
#define SIZE_OF_SQL_REQUEST 300
#define TIME_TO_RESEND       20 /* Minimum time between same alert is rerun */

/* Get SQL */
static int get_value_as_int
(void *data, int argc, char **argv, char **azColName) {
  if (argv[0])
    *((int *)data) = atoi(argv[0]);
  else *((int *)data) = -1;
  return 0;
}

/**
 * Send Mail
 *
 * Send mail due to the alert of index passed in parameter
 * params : int function : index of the alert in the sql table alertes
 */
int send_mail(int function) {
  char argument[10];
  snprintf(argument, 10, "%d", function);
  if (fork() == 0) {
    execlp("./send_mail.sh","./send_mail.sh", argument, NULL);
    return -1;
  }
  return 0;
}

/**********************************************************************
 * Does the function must be launched ?                               *
 *                                                                    *
 * List of the functions that will be part of the routine table.      *
 * Each function is designed to one alert, and each function must     *
 * return a bool answering to the question : Does that alert must be  *
 * launched ? Answering to conditional value, different for each      *
 * funtions.                                                          *
 *********************************************************************/

/**
 * That function is called by all routine function
 * The parameters contains all the condition neede to be checked int
 * the sql table to calculate if the alarm is required to be launched,
 * or not
 */
int test_alert
 (sqlite3 *db,
  int module_number,
  int sensor_type,
  int sensor_limit_over,
  int sensor_limit_under,
  int hour_limit_over,
  int minute_limit_over,
  int hour_limit_under,
  int minute_limit_under) {
    int sensor, hour, minute;
    int id = -1;
    char sql_request[SIZE_OF_SQL_REQUEST];
    /* Get id of last request */
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "SELECT id FROM main ORDER BY ID DESC LIMIT 1;");
    sqlite3_exec(db, sql_request, get_value_as_int, &id, NULL);
    if (id == -1) return 0;
    /* Get hour of last request */
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "select strftime(%s ,date) from main where id = %d", "'%H'", id);
    sqlite3_exec(db, sql_request, get_value_as_int, &hour, NULL);
    if (hour < hour_limit_under) return 0;
    if (hour > hour_limit_over) return 0;
    /* Get minute of last request */
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "select strftime(%s ,date) from main where id = %d", "'%M'", id);
    sqlite3_exec(db, sql_request, get_value_as_int, &minute, NULL);
    if (minute < minute_limit_under) return 0;
    if (minute > minute_limit_over) return 0;
    /* Get value of last request sensor */
    if (sensor_type == ST_PIR)
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "select (ifnull(mod%d_pir0, 0)+ifnull(mod%d_pir1, 0)+ifnull(mod%d_pir2, 0)+ifnull(mod%d_pir3, 0)+ifnull(mod%d_pir4, 0)+ifnull(mod%d_pir5, 0)+ifnull(mod%d_pir6, 0)+ifnull(mod%d_pir7, 0)+ifnull(mod%d_pir8, 0)+ifnull(mod%d_pir9, 0)) from main where id = %d;",
      module_number, module_number, module_number, module_number, module_number, module_number, module_number, module_number, module_number, module_number, id);
    else if (sensor_type == ST_LIGHT)
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "select ifnull(mod%d_light, 0) from main where id = %d;", module_number, id);
    else if (sensor_type == ST_SOUND)
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "select ifnull(mod%d_light, 0) from main where id = %d;", module_number, id);
    else return -1;
    /* SQL Request */
    sqlite3_exec(db, sql_request, get_value_as_int, &sensor, NULL);
    /* Check for value */
    if (sensor < sensor_limit_under) return 1;
    if (sensor > sensor_limit_over) return 1;
    return 0;  
}

/* Function 1 : Test Alert : Always true */
int test_alert(sqlite3 *db) { return 1; }

/**
 * Routine Table
 * Function must be inserted with the same order than theirs alerts
 * are defined in the sql/alertes table
 */
int (*does_function_must_be_run[NUMBER_OF_ALERT])(sqlite3 *db) = {
};

/**
 * Function that check if an alert must be triggered, and run it if so
 * params : sqlite3 *db  : The database containing the alerts
 *          int function : The index of the alert to check in the database
 *          int status   : The status of the alert to check n the database
 */
int start_function(sqlite3 *db, int function, int status) {
  int to_launch;
  char sql_request[SIZE_OF_SQL_REQUEST];
  /* Return now if the function don't have to be launched */
  if (status == 0) return 0;
  /* Has is been launched recently ? */
  if (status > 2) {
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "UPDATE alertes SET status = %d WHERE id = %d;", status - 1, function);
    sqlite3_exec(db, sql_request, NULL, NULL, NULL);
    return 0;
  }
  /* Must function be launched ? */
  if (does_function_must_be_run[function - 1] == 0)
    {printf("ERROR 1");return 1;}
  to_launch = (*does_function_must_be_run[function - 1])(db);
  if (to_launch == 0) return 0;
  /* Launch alert */
  printf("Function %d triggered.", function);
  send_mail(function);
  /* Update database if function had to be launched once */
  if (status == 1) {
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "UPDATE alertes SET status = 0 WHERE id = %d;", function);
    sqlite3_exec(db, sql_request, NULL, NULL, NULL);
  }
  /* Update database if function had to be launched many time */
  if (status == 2) {
    snprintf(sql_request, SIZE_OF_SQL_REQUEST,
      "UPDATE alertes SET status = %d WHERE id = %d;",
      status + TIME_TO_RESEND, function);
    sqlite3_exec(db, sql_request, NULL, NULL, NULL);
  }
  return 0;
}

#endif