/**
 * Programme reception.
 * Ce programme doit scruter la NRF24 et dès qu'un message est reçu, l'envoyer
 * au programme passeur via un tube.
 */
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <RF24/RF24.h>
#include <ctime>

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include "module.h"
#include "fail.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

typedef uint8_t byte;

using namespace std;

RF24 radio(15,8); // radio(CE,CS)
int entreeTube = -1;
char buffer[MESSAGE_SIZE];
byte addresses[][6] = {"xxxxx"};
char __pr_name_[] = "NRF Reception : ";

void radiosetup() {
  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
}

/**
 * Main program to seek for radio input.
 * The program must be launched with as argument the nRF24 canal to look after
 */
int main(int argc, char const *argv[])
{
  /* Signal interrupt to close the program cleanly with a CTRL+C */
  printf("%sStart\n", __pr_name_);
  if (argc < 1)
    fail((char *)"Must get radio channel as argument.\n", __pr_name_);
  strncpy((char *)addresses[0], argv[1], 5);
  printf("%sGet channel %s\n", __pr_name_, addresses[0]);  

  printf("%sOpen fifo\n", __pr_name_);
  char nomTube[] = "/tmp/psesi.fifo";
  if(mkfifo(nomTube, 0644) != 0) 
    fail((char *)"Unable to create pipe\n", __pr_name_);
  if((entreeTube = open(nomTube, O_WRONLY)) == -1) 
    fail((char *)"Unable to open pipe\n", __pr_name_);

  printf("%sOpen radio\n", __pr_name_);
  radiosetup();

  printf("%sStart reading radio\n", __pr_name_);
  radio.openReadingPipe(1,addresses[0]);
  radio.startListening();

  while(1) {
    sleep(1);
  if( radio.available() ) {
    radio.read( buffer, MESSAGE_SIZE );             // Get the payload
    printf("%sMessage received : ", __pr_name_);
    for (int i = 0; i < MESSAGE_SIZE; ++i)
      printf("%c", buffer[i]);
    printf("\n");
   }
   write(entreeTube, buffer, MESSAGE_SIZE);
  }
  return 0;
}