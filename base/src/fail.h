#ifndef __SB_FAIL_H__
#define __SB_FAIL_H__ 0

#include <stdio.h>
#include <stdlib.h>

int fail(char * arg, char * __pr_name_) {
  printf("%s%s", __pr_name_, arg);
  exit(EXIT_FAILURE);
}
#endif