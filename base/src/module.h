#ifndef __SB_MODULE_H__
#define __SB_MODULE_H__ 0
/**
 * Le format établit pour écrire dans le tube est le suivant :
 * Pour Chaque Module :
 *   16 premiers caractères : Nom unique du module
 *   10 caractères suivants : Valeur de capteur PIR
 *    4 caractères suivants : Valeur du capteur de son
 *    4 caractères suivants : Valeur du capteur de lumière
 * Message pour le module suivant écrit à la suite du précédent.
 *
 * Une valeur absente vaut xxxx, x dans le cas d'un pir.
 *
 * Voilà donc un exemple de message :
 * "Chambre 1     010x1101285 148 "
 *
 */
#include <string.h>
#include <stdio.h>

#define PIR_ARRAY_SIZE           10
#define MESSAGE_SIZE             32
#define SIZE_OF_NAME_MESSAGE     15
#define SIZE_OF_COUNTER_MESSAGE   0
#define SIZE_OF_CAPTOR_MESSAGE    5
#define MODULE_STATUS_VALID       1

/**
 * Struct describing a module status
 */
struct Module {
  char nom[SIZE_OF_NAME_MESSAGE];
  int cpt;
  int pir[PIR_ARRAY_SIZE];
  int son;
  int lumiere;
  int status;
};

/**
 * Execute a function given in parameter to all the struct Module array
 * passed in parameter
 */
void callback_on_module_array
(struct Module module[], int size, void fn(struct Module * module)) {
  for (int i = 0; i < size; ++i)
    fn(&module[i]);  
}

/* Set the status of the module passed in parameter to unvalid */
void reset_module(struct Module * module) {
  module->status = !MODULE_STATUS_VALID;
}

/* Set the status of the module passed in parameter to valid */
void set_module_valid(struct Module * module) {
  module->status = MODULE_STATUS_VALID;
}

/* Return true if the status of the module passed in parameter is valid */
int module_exist(struct Module * module) {
  return module->status;
}

/**
 * Change the value of the module passed in parameter according to the string
 * passed in parameter. That string is formated by the module according to the
 * description in the header of the current file
 */
void update_module(struct Module * module, char * pipe_message) {
  char buffer[20];
  /* Name */
  strncpy(module->nom, pipe_message, SIZE_OF_NAME_MESSAGE);
  pipe_message += SIZE_OF_NAME_MESSAGE;
  /* PIR captors */
  for (int i = 0; i < PIR_ARRAY_SIZE; ++i)
    if      (pipe_message[i] == '0') module->pir[i] = 0;
    else if (pipe_message[i] == '1') module->pir[i] = 1;
    else                             module->pir[i] = -1;
  pipe_message += PIR_ARRAY_SIZE;
  /* Sound captor */
  strncpy(buffer, pipe_message, SIZE_OF_CAPTOR_MESSAGE);
  module->son = atoi(buffer);
  pipe_message += SIZE_OF_CAPTOR_MESSAGE;
  /* Light captor */
  strncpy(buffer, pipe_message, SIZE_OF_CAPTOR_MESSAGE);
  module->lumiere = atoi(buffer);
  pipe_message += SIZE_OF_CAPTOR_MESSAGE;
}

/* Print the module to standard output */
void print_module(struct Module * momo) {
  printf("%s ", momo->nom);
  printf("%d ", momo->cpt);
  for (int i = 0; i < PIR_ARRAY_SIZE; ++i)
    printf("%d ", momo->pir[i]);
  printf("%d %d %d\n", momo->son, momo->lumiere, momo->status);
}

#endif