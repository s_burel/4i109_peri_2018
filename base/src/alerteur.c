/**
 * That programm check, one time per minutes, if it has to trigger an alert
 * due to the sql database
 **/
#include "alert.h"

#define SIZE_SQL_REQUEST  4001

/* Callback of a sql request to store an alert value from the table alert */
static int get_alerts_value
(void *data, int argc, char **argv, char **azColName) {
  int * array = (int *)data;
  int index = atoi(argv[0]);
  return 0;
}

/* This program is ran one time per minute */
int main(int argc, char const *argv[])
{
  int seconds;
  time_t _time;
  struct tm instant;
  sqlite3 *db;
  char sql_request[SIZE_SQL_REQUEST];
  int alert[NUMBER_OF_ALERT];
  sqlite3_open("db", &db);
  /**
   * This program is ran one time per minute
   */
  while(1) {
    sleep(1);
    time(&_time);
    instant=*localtime(&_time);
    seconds = instant.tm_sec;
    if (seconds != 30) continue;
    /* Main program */
    /* Get list of functions status */
    strcpy(sql_request, "SELECT id, status from alertes");
    sqlite3_exec(db, sql_request, get_alerts_value, alert, NULL);
    /* For each functions : launch routine */
    for (int i = 0; i < NUMBER_OF_ALERT; ++i) {
        start_function(db, i, alert[i]);
    }
  }
  return 0;
}