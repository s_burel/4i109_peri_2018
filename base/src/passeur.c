#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sqlite3.h>
#include <signal.h>
#include "fail.h"
#include "module.h"
#include "sql_request.h"

#define SIZE_SQL_REQUEST  4000
#define NUMBER_OF_MODULES   10
#define DEBUG                0

char __pr_name_[] = "Pipe to SQL : ";
struct Module module[NUMBER_OF_MODULES];
char sql_request[SIZE_SQL_REQUEST];
int minute;
char pipe_message[MESSAGE_SIZE];
sqlite3 *db;

/**
 * Search for a module with matching name in passed array
 * return a pointer to the Module found, or NULL if no Module was found
 */
struct Module * get_module_from_name
  (struct Module * array, int size, char * name) {
  for (int i = 0; i < size; ++i)
    if (strncmp((&(array[i]))->nom, name, SIZE_OF_NAME_MESSAGE) == 0)
      return &array[i];
  return NULL;
}

/* Reset all the module staus of the module array */
void reset_modules_and_pipe_buffer() {
  callback_on_module_array(module, NUMBER_OF_MODULES, reset_module);
  pipe_message[0] = 0;
}

/**
 * Update module according to new message from the pipe
 * and update the sql database if a new minute occured
 */
void update_db(int sig)
{
  if (DEBUG) printf("%sUpdate the DB : Start\n", __pr_name_);
  signal(SIGALRM, SIG_IGN);          /* ignore this signal       */
  struct Module * momo;

  /* Get time */
  time_t secondes;
  struct tm instant;
  time(&secondes);
  instant=*localtime(&secondes);

  /* Update Modules */
  if (pipe_message[0] == 0) {
    if (DEBUG) printf("%s No new message\n", __pr_name_);
    goto end;
  }
    if (DEBUG) printf("%s Update module\n", __pr_name_);
  momo = get_module_from_name(module, NUMBER_OF_MODULES, pipe_message);
  if (momo == NULL) {
    printf("%s Wrong Module name from the nrf\n", __pr_name_);
    goto end;
  }
  update_module(momo, pipe_message);
  set_module_valid(momo);

  /* New minute mean update the sql table*/
  if (minute != instant.tm_min) {
    printf("%s Update the SQL Table\n", __pr_name_);
    minute = instant.tm_min;
    /* Build the sql request */
    build_sql_request(sql_request, SIZE_SQL_REQUEST, module);
    printf("%s\n", sql_request);
    sqlite3_exec(db, sql_request, insert_sql, (void*)sql_request, NULL);
    /* Reset modules and message */
    reset_modules_and_pipe_buffer();
  }

  end:
  alarm(1);
  signal(SIGALRM, update_db);     /* reinstall the handler    */
}

/* SQL callback. Set the name of passed module */
static int set_module_name(
  void *data, int argc, char **argv, char **azColName) {
  printf("Set name");
  strcpy(((struct Module *)data)->nom, argv[0]);
}

/* That program read the pipe continually and update the datbase one time
 * per minute
 */
int main(void)
{
  time_t secondes;
  struct tm instant;
  int sortieTube;

  printf("%sStart\n", __pr_name_);

  /* Set time */
  time(&secondes);
  instant=*localtime(&secondes);
  minute = instant.tm_min;

  /* Set pipe */
  printf("%sOpen fifo\n", __pr_name_);
  char nomTube[] = "/tmp/psesi.fifo";
  if((sortieTube = open (nomTube, O_RDONLY)) == -1) 
    fail((char *)"Impossible d'ouvrir la sortie du tube nommé.\n", __pr_name_);

  /* Build module */
  printf("%sBuild modules\n", __pr_name_);
  callback_on_module_array(module, NUMBER_OF_MODULES, reset_module);
  const char* data = "Callback function called";
  if(sqlite3_open("db", &db) != 0) 
    fail((char *)"Impossible d'ouvrir la base de donnée.\n", __pr_name_);
  /* Module 1 */
  strcpy(sql_request, "SELECT name from modules where id = 1;");
  sqlite3_exec(db, sql_request, set_module_name, (void*)&module[0], NULL);
  reset_module(&module[0]);
  /* Module 2 */
  strcpy(sql_request, "SELECT name from modules where id = 2;");
  sqlite3_exec(db, sql_request, set_module_name, (void*)&module[1], NULL);
  reset_module(&module[1]);

  /* Set Signal */
  read(sortieTube, pipe_message, MESSAGE_SIZE);
  update_db(0);

  while(1) {
  /* Set Signal */
  if (DEBUG) printf("%sWait for pipe input\n", __pr_name_);
  read(sortieTube, pipe_message, MESSAGE_SIZE);
  update_db(0);
  sleep(1);
  }

  sqlite3_close(db);

  return EXIT_SUCCESS;
}