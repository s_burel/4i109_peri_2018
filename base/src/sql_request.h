#ifndef __SB_SQL_REQUEST_H__
#define __SB_SQL_REQUEST_H__ 0

#include <string.h>
#include <stdio.h>
#include "module.h"

#define NUMBER_MAX_MODULE_IN_SQL_TABLE 10

/* Build a sql request used by reception.cpp according to the module array */
void build_sql_request(char * sql_request, int size, void * array) {
  struct Module * modules = (struct Module *)array;
  char * replace;
  snprintf(sql_request, size,
    "insert into main values (null, DateTime('now'),\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,\
   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d,   %d);\n",
      /* Module 0 */
      module_exist(&modules[0]) ? modules[0].pir[0]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[1]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[2]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[3]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[4]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[5]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[6]  : -1,
      module_exist(&modules[0]) ? modules[0].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[0]) ? modules[0].lumiere : -1,
      module_exist(&modules[0]) ? modules[0].son     : -1,
      /* Module 1 */
      module_exist(&modules[1]) ? modules[1].pir[0]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[1]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[2]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[3]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[4]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[5]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[6]  : -1,
      module_exist(&modules[1]) ? modules[1].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[1]) ? modules[1].lumiere : -1,
      module_exist(&modules[1]) ? modules[1].son     : -1,
      /* Module 2 */
      module_exist(&modules[2]) ? modules[2].pir[0]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[1]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[2]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[3]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[4]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[5]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[6]  : -1,
      module_exist(&modules[2]) ? modules[2].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[2]) ? modules[2].lumiere : -1,
      module_exist(&modules[2]) ? modules[2].son     : -1,
      /* Module 3 */
      module_exist(&modules[3]) ? modules[3].pir[0]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[1]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[2]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[3]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[4]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[5]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[6]  : -1,
      module_exist(&modules[3]) ? modules[3].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[3]) ? modules[3].lumiere : -1,
      module_exist(&modules[3]) ? modules[3].son     : -1,
      /* Module 4 */
      module_exist(&modules[4]) ? modules[4].pir[0]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[1]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[2]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[3]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[4]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[5]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[6]  : -1,
      module_exist(&modules[4]) ? modules[4].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[4]) ? modules[4].lumiere : -1,
      module_exist(&modules[4]) ? modules[4].son     : -1,
      /* Module 5 */
      module_exist(&modules[5]) ? modules[5].pir[0]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[1]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[2]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[3]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[4]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[5]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[6]  : -1,
      module_exist(&modules[5]) ? modules[5].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[5]) ? modules[5].lumiere : -1,
      module_exist(&modules[5]) ? modules[5].son     : -1,
      /* Module 6 */
      module_exist(&modules[6]) ? modules[6].pir[0]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[1]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[2]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[3]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[4]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[5]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[6]  : -1,
      module_exist(&modules[6]) ? modules[6].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[6]) ? modules[6].lumiere : -1,
      module_exist(&modules[6]) ? modules[6].son     : -1,
      /* Module 7 */
      module_exist(&modules[7]) ? modules[7].pir[0]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[1]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[2]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[3]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[4]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[5]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[6]  : -1,
      module_exist(&modules[7]) ? modules[7].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[7]) ? modules[7].lumiere : -1,
      module_exist(&modules[7]) ? modules[7].son     : -1,
      /* Module 8 */
      module_exist(&modules[8]) ? modules[8].pir[0]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[1]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[2]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[3]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[4]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[5]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[6]  : -1,
      module_exist(&modules[8]) ? modules[8].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[8]) ? modules[8].lumiere : -1,
      module_exist(&modules[8]) ? modules[8].son     : -1,
      /* Module 9 */
      module_exist(&modules[9]) ? modules[9].pir[0]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[1]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[2]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[3]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[4]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[5]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[6]  : -1,
      module_exist(&modules[9]) ? modules[9].pir[7]  : -1,
      -1, -1,
      module_exist(&modules[9]) ? modules[9].lumiere : -1,
      module_exist(&modules[9]) ? modules[9].son     : -1);

  while (replace = strstr (sql_request,"  -1"))
    strncpy (replace,"null",4);
}

/* Simple Callback used after an sql insert to display the insert */
static int insert_sql(void *data, int argc, char **argv, char **azColName) {
  printf("SQL Insert : %s\n", (char *) data);
}

#endif